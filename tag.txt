このテキストファイルにはタグの使い方を記述していますREAD ME にも書いてありますが
その部分だけを適当に抜粋したと思って大丈夫です(Sakaki)

——————————————————
・参考文献
文中に入れるタグ　
文書
例：¥cite{magazin}
reference.texに書くタグ
　例：¥bibmagazin{magazin}{山上一郎, 山下二郎}{パラメトリック増幅器}{進学論（B）}{vol.J62-B}{no.1}{20-27}{Jan, 1979}

web
例：¥cite{web01}
reference.texに書くタグ
  ¥bibweb{web01}{総務省}{平成27年版情報通信白書}{http://www.soumu.go.jp/johotsusintokei/whitepaper/ja/h27/html/nc372110.htmll}{Nov 22，2016}
——————————————————————————————————————
・図形
図x:¥figref{figure1}
図表示：¥figPost[図目次に表示される名前(省略化)]{7}{figure1}{圧縮率と伝送速度
図はgifure/に保存
並列は：¥figParallel{0.5}{figure1}{図1}{zu1}{0.5}{figure1}{図2}{zu2}
強制的に記述位置に画像挿入したい場合は
\figForced{3}{figure1}{圧縮率と伝送速度}
———————————————————————————————————
・表
表x: ¥tabref{sample}
内容：
begin{table}[htbp]
 ¥begin{center}
  ¥caption[ここは表目次の名前(省略可)]{AとBとCとD}
  ¥begin{tabular}{|c|c|} ¥hline
   AAAAA & BBBBB ¥¥ ¥hline
   CCCCC & DDDDD ¥¥ ¥hline
  ¥end{tabular}
  ¥label{tab:sample}
 ¥end{center}
¥end{table}
—————————————————————————

ソースコード追加
まずconfのconf.txtにある
¥SRCfalseを¥SRCtrueに変更
src/listingsフォルダをC:\w32tex\share\texmf-dist\tex\latex\misc
にコピー※環境によってLatexの入れた環境にいれて
使う時は
¥ifSRC
    ソースコードを参照する場合は¥srcref{sample}のように参照する．
    ¥srcPost{sample.java}{sample}{sample.javaのソースコード}
¥fi
を記入する。
——————————————————————————————————

Listの使い方

\begin{itemize}
 \item 開発ツール
    \begin{itemize}
      \item Mac OS ???
      \item Android Studio(ver ???)
      \item Android SDK
     \end{itemize}

\item 言語
  \begin{itemize}
    \item Java
    \item HTML5
    \item JavaScript
  \end{itemize}
\end{itemize}

————————————————
新たなるページに移動する,図とかで調整する時に優秀ニキ
\newpage
